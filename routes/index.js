var express = require('express');
var router = express.Router();
var db = require('../dbconnection');

/* GET home page. */
router.get('/', function(req, res, next) {
  db.query("SELECT * FROM fv_articles ",(err, rows)=>{
    if(err){
      res.redirect('/');
    }
    res.render('index/list', { title: 'Express Listing', data:rows });
  });
});

module.exports = router;

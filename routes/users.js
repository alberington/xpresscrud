var express = require('express');
var router = express.Router();
var db = require('../dbconnection');

/* GET users listing. */
const ttl = 'Express';
router.list = function(req, res, next) {
  // res.json({"data" : req.query.start});
  try {
    var start = req.query.start;
    
    db.query("SELECT * FROM fv_authors LIMIT 0, 5",(err, rows)=>{
      if(err){
        res.json( {"status" : "Error"} );
      }
      res.json( {"data" : rows} );
    });
  } catch (error) {
    res.json({"data" : "error"});
  }
};

router.user = function(req, res, next) {
  try {
    
  } catch (error) {
    
  }
};

router.add = function (req,res, next) {
  res.render('index/add', {title:ttl, mode:'Add User',});  
};

router.insert =  function (req,res, next) {
  data = {
    name : req.body.name,
    email : req.body.email,
    password : req.body.password,
    role : req.body.role,
    status : req.body.status
  };
  
  query = db.query("INSERT INTO fv_authors set ? ",data, function(err, rows)  {
    if (err){
      res.json({"data" : err});
    } else {
      res.json({"data" : "success"});
    }
  });
};

router.edit = function(req, res, next){
  let id = req.params.id;
  db.query("SELECT * FROM fv_authors WHERE id = ?", [id], (err, rows)=>{
    if(err)
    // res.send(rows);
        console.log("Error Selecting : %s ",err );
    res.render('index/add',{page_title:"Edit Author",author:rows});
  //   console.log(rows);
  });
};

router.delete_customer = function(req,res){          
  let id = req.params.id;
     db.query("DELETE FROM fv_authors  WHERE id = ? ",[id], function(err, rows){
      if(err)
          console.log("Error deleting : %s ",err );
      res.redirect('/');
     });
};


module.exports = router;

var db = require('../dbconnection');

var User = {
    getTotalUsers: function(callback) {
        return db.query("SELECT COUNT(id) FROM fv_authors", callback);
    },

    getUsers: function(start, callback) {
        return db.query("SELECT * FROM fv_authors LIMIT ?, 10",[start], callback);
    },

    getUser: function(id, callback) {
        return db.query("SELECT * FROM fv_authors WHERE id=?",[id], callback);
    },

    createUser: function(User, callback) {
        return db.query("INSERT INTO fv_authors VALUES(?,?,?,?)",[User.name, User.email, User.password, User.role], callback);
    },

    updateUser: function(id, User, callback) {
        return db.query("UPDATE fv_authors SET name=?, email=?, password=?, role=?, status=? WHERE id=?", [User.name, User.email, User.password, User.role, User.status, id], callback);
    },

    deleteUser: function(id, callback) {
        return db.query("UPDATE fv_authors SET status=?",[id], callback);
    }
};

module.exports=User;